interface Handler {
  setNext(handler: Handler): Handler;

  handle(sum: number): number;
}
abstract class Discount implements Handler {
  private nextHandler?: Handler;

  public setNext(handler: Handler): Handler {
    this.nextHandler = handler;
    return handler;
  }

  public handle(sum: number): number {
    if (this.nextHandler) {
      return this.nextHandler.handle(sum);
    }
    return sum;
  }
}
// 9.5
class Discount_95 extends Discount {
  public handle(sum: number): number {
    if (sum > 100 && sum <= 200) {
      process.stdout.write('9.5折');
      return sum * 0.95;
    }
    return super.handle(sum);
  }
}
// 9
class Discount_9 extends Discount {
  public handle(sum: number): number {
    if (sum > 200 && sum <= 300) {
      process.stdout.write('9折');

      return sum * 0.9;
    }
    return super.handle(sum);
  }
}
// 8.5
class Discount_85 extends Discount {
  public handle(sum: number): number {
    if (sum > 300 && sum <= 400) {
      process.stdout.write('8.5折');

      return sum * 0.85;
    }
    return super.handle(sum);
  }
}
// 8
class Discount_8 extends Discount {
  public handle(sum: number): number {
    if (sum > 400 && sum <= 500) {
      process.stdout.write('8折');

      return sum * 0.8;
    }
    return super.handle(sum);
  }
}
const discount_95 = new Discount_95();
const discount_9 = new Discount_9();
const discount_85 = new Discount_85();
const discount_8 = new Discount_8();

discount_95.setNext(discount_9).setNext(discount_85).setNext(discount_8);

console.log(discount_95.handle(199));
console.log(discount_95.handle(210));
console.log(discount_95.handle(310));
console.log(discount_95.handle(410));
