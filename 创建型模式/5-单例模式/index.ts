/**
 * 单例模式是一个类只有一个实例,并提供一个全局访问
 */

// 登录对话框
class Login {
  private state: 'hide' | 'show';
  constructor() {
    this.state = 'hide';
  }
  public hide(): void {
    if (this.state === 'show') {
      this.state = 'hide';
    }
    console.log('无需重复隐藏');
    return;
  }
  public show(): void {
    if (this.state === 'hide') {
      this.state = 'show';
    }
    console.log('无需重复显示');
    return;
  }
}

// 代理实现单例模式
const CreateLogin = (() => {
  let instance: Login | undefined;
  return () => {
    if (instance) {
      return instance;
    }
    instance = new Login();
    return instance;
  };
})();

const login = CreateLogin();
const login_1 = CreateLogin();

console.log(login === login_1);
