// 保护代理：
// 虚拟代理:将一些花销较大的对象延迟到需要时创建

class MyImage {
  private imageNode: HTMLImageElement;
  constructor() {
    this.imageNode = document.createElement('img');
    document.appendChild(this.imageNode);
  }
  public setSrc(src: string) {
    this.imageNode.src = src;
  }
}

class ProxyMyImage {}
